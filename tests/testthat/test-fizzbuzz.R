test_that("test_1", {
  expect_equal(fizzbuzz(1), "1")
})

test_that("test_2", {
  expect_equal(fizzbuzz(2), "2")
})

test_that("test_3", {
  expect_equal(fizzbuzz(3), "fizz")
})

test_that("test_4", {
  expect_equal(fizzbuzz(4), "4")
})

test_that("test_5", {
  expect_equal(fizzbuzz(5), "buzz")
})

test_that("test_6", {
  expect_equal(fizzbuzz(6), "fizz")
})

test_that("test_10", {
  expect_equal(fizzbuzz(10), "buzz")
})

test_that("test_15", {
  expect_equal(fizzbuzz(15), "fizzbuzz")
})

test_that("test_30", {
  expect_equal(fizzbuzz(30), "fizzbuzz")
})

test_that("test_cc_0", {
  expect_equal(fizzbuzz(0), "fizzbuzz")
})

test_that("test_cc_-1", {
  expect_equal(fizzbuzz(-1), "-1")
})

test_that("test_cc_-15", {
  expect_equal(fizzbuzz(-15), "fizzbuzz")
})

test_that("test_cc_non_numeric", {
  expect_error(fizzbuzz("test"), "ERROR: please enter only numeric values!")
})



